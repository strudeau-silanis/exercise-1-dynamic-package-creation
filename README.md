# README #

## Compiling the Application ##

This Sample Insurance Application is a standard java web application that was tested to run on tomcat 7, openshift (tomcat 7), heroku (jetty 9.x). In order to compile and run for the right deployment environment, using MAVEN enable the profile corresponding to your environment.

- tomcat
- openshift
- heroku

E.g.:

```
$ mvn -P tomcat clean package
```

### Note ###

You don't need to explicitly specify a maven profile when deploying to openshift or heroku, the code is configured to use the right profile automatically.

## How To Deploy on HEROKU ##

1. Create an heroku account at: [www.heroku.com](www.heroku.com)
2. Download and install the [heroku tool belt](https://toolbelt.heroku.com/). This will install Heroku CLI and GIT command line client.
3. Create a root directory for this project on your computer.
4. Checkout the project using git:

        git clone https://bitbucket.org/strudeau-silanis/exercise-1-dynamic-package-creation.git

5. From this project root directory referred to as `$` in the example below.
6. Login and create an SSH key:

        $ heroku login
        Enter your Heroku credentials.
        Email: adam@example.com
        Password:
        Could not find an existing public key.
        Would you like to generate one? [Yn]
        Generating new SSH public key.
        Uploading ssh public key /Users/adam/.ssh/id_rsa.pub
	
7. Create an app on Heroku:

        $ heroku create

8. Define a MAVEN settings file to use the heroku maven profile

        $ heroku config:set MAVEN_SETTINGS_PATH=.heroku/settings.xml
	
9. Push your application to heroku. You should now see your code being uploaded to heroku and the application being built.

        $ git push heroku master

10. Open the application using the system default browser.

        $ heroku open

11. Repeat step 9-10 above if you make code changes.
12. Congratulation, your application should now be running in heroku!