<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/eSLSampleWebApp.css"/>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
 function gotoCongratulation(){
	 window.location.replace("Congratulations");
 }
 function gotoInterruptedSigning(){
	 window.location.replace("InterruptedSigning");
 }
</script>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href='<c:url value="index"/>'>JAVA e-SL Application</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href='<c:url value="InsuranceForm"/>'>Insurance Form</a></li>
					<li><a href='<c:url value="ESLNotifications"/>'>Notifications Dashboard</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container body-content">
		<div align="center">
	        <iframe src="https://sandbox.e-signlive.com/access?sessionToken=${token}" width="1050" height="1250" scrolling="yes"></iframe>
	    </div>
	    <hr />
		<footer>
			<p>&copy; 2014 -JAVA e-SL Application</p>
		</footer>
    </div>
</body>
</html>
