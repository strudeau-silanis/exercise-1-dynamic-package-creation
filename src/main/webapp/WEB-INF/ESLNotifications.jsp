<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/eSLSampleWebApp.css"/>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<title>Interrupted Signing Process</title>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href='<c:url value="index"/>'>JAVA e-SL Application</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href='<c:url value="InsuranceForm"/>'>Insurance Form</a></li>
					<li><a href='<c:url value="ESLNotifications"/>'>Notifications Dashboard</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container body-content">
		<h2>Dashboard</h2>
		<table class="table table-hover table-bordered" style="border-width: 3px; border-color: ForestGreen; border-style: solid">
			<tr>
				<th>Package ID</th>
				<th>Package Status</th>
				<th>Available Actions</th>
			</tr>
	
			<c:forEach items="${myMap}" var="entry">
				<tr>
					<td>${entry.key}</td>
					<!--  key alone will get displayed if I comment the 'entry.values' iteration forEach loop-->
					<td>${entry.value}</td>
					<td><c:if
							test="${entry.value == 'PACKAGE_COMPLETE' || entry.value == 'SIGNER_COMPLETE' }">
							<a href="downloadDocuments?packageId=${entry.key}"	class="btn btn-success">Download Signed PDF</a>
							<a href="downloadEvidenceSummary?packageId=${entry.key}" class="btn btn-success">Download Evidence Summary</a>
							<a href="getFieldValues?packageId=${entry.key}"	class="btn btn-success">Get Field Values</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
		<hr />
		<footer>
			<p>&copy; 2014 -JAVA e-SL Application</p>
		</footer>
	</div>
</body>
</html>
