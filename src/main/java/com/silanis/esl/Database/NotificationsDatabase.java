package com.silanis.esl.Database;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

//Stores the e-SignLive notifications in a database.
//Stores the package id along with its status in a database.
public class NotificationsDatabase {

	// Creates an object of NotificationsDatabase
	private static NotificationsDatabase instance;
	private static Cache<String, String> cache = CacheBuilder.newBuilder().maximumSize(100).build();
	private static final Logger log = LoggerFactory.getLogger(NotificationsDatabase.class);
	
	// constructor private. Class can't be instantiated
	private NotificationsDatabase() {}

	// Gets the only object available
	// Synchronized in case access method called twice from 2 different class and more than one object is created.
	public static synchronized NotificationsDatabase getInstance() {
		if (instance == null) {
			instance = new NotificationsDatabase();
		}
		return instance;
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
	//Saves the package Id and the status to the database
	public void save(String packageId, String status){
		cache.put(packageId, status);
		log.info("Here is your e-SignLive notification:"+ status );
		log.info("For the following package Id:" + packageId);
	}
			      
	//Returns the status associated with the packageId
	public String getStatus(String packageId){
		String status = cache.getIfPresent(packageId);
		return status;
	}  
	
	public ConcurrentMap<String, String> getMap(){
		return cache.asMap();	
	}
}


	
