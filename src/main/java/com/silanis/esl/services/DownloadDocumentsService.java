package com.silanis.esl.services;

import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.PackageId;

//Downloads the signed documents and the documents zip file
public class DownloadDocumentsService {

	public byte[] downloadZippedDocuments(String packageId, String API_KEY, String API_URL) {
		EslClient eslClient = new EslClient(API_KEY, API_URL);

		PackageId pckId = new PackageId(packageId);
		byte[] zip = eslClient.downloadZippedDocuments(pckId);
		return zip;

	}
}
