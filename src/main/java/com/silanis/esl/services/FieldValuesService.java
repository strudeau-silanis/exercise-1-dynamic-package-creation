package com.silanis.esl.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.FieldSummary;
import com.silanis.esl.sdk.PackageId;

//Retrieves the values found in the fields of each of the signed documents in a package
public class FieldValuesService {
	private final Logger log = LoggerFactory.getLogger(FieldValuesService.class);
	
	public String getFieldValues(String packageId, String API_KEY, String API_URL){	
		log.debug("Printing Field Values for package: "+packageId);
		EslClient esl = new EslClient( API_KEY, API_URL );
		
		PackageId pckId = new PackageId(packageId);
		List<FieldSummary> fieldContent = esl.getFieldValues( pckId );
	
		StringBuilder sb = new StringBuilder();
		for ( FieldSummary fieldSummary : fieldContent ) {
			sb.append("signerId: "+fieldSummary.getSignerId() + 
					  ", documentId: " + fieldSummary.getDocumentId() + 
					  ", fieldId: " + fieldSummary.getFieldId() + 
					  ", fieldValue:" + fieldSummary.getFieldValue() + "\n" );
		}

		return sb.toString();
	}
}