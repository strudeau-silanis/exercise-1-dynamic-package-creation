package com.silanis.esl.services;

import com.silanis.esl.Database.NotificationsDatabase;
import com.silanis.esl.models.ESLNotificationsModel;
import com.silanis.esl.models.NotificationEventsType;


// Registers and saves the event notifications
public class ESLNotificationsService {
		public void saveToDB(ESLNotificationsModel eslNotification) {
		NotificationsDatabase database = NotificationsDatabase.getInstance();
		String notificationName = eslNotification.getName();
		String notificationPackageId = eslNotification.getPackageId();
		
		
		//Package has been sent to be signed
        if(notificationName.contentEquals(NotificationEventsType.PACKAGE_ACTIVATE.toString())){
        	database.save(notificationPackageId,notificationName );
       	}
        
       //Package has been signed and confirmed by all signers, and marked as complete 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_COMPLETE.toString())){
           	database.save(notificationPackageId,notificationName );
        }
 
        //Package has passed its expiry date and been deactivated 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_EXPIRE.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
       //Package has deactivated because one of the signers has opted out of the electronic signing process 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_OPT_OUT.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
    	//Package has deactivated because one of the signers does not want to sign
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_DECLINE.toString())){
           	database.save(notificationPackageId,notificationName );
        }
        
      	//Signer has completed signing all documents 
        else if(notificationName.contentEquals(NotificationEventsType.SIGNER_COMPLETE.toString())){
        	database.save(notificationPackageId,notificationName );
        }

      	//Document has been signed by one of the signers 
        else if(notificationName.contentEquals(NotificationEventsType.DOCUMENT_SIGNED.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
     	//A signer has reassigned their signing responsibilities to a new signer
        else if(notificationName.contentEquals(NotificationEventsType.ROLE_REASSIGN.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been created
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_CREATE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        else {
        	throw new RuntimeException("Unhandled eSL notification: "+notificationName);
        }
       
	}
}



