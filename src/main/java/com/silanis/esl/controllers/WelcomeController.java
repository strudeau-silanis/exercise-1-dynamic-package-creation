package com.silanis.esl.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// Displays the "Welcome Page" of the insurance company web application where the user is promoted to start filling his insurance form 

public class WelcomeController extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	public WelcomeController() {
		super();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = req.getRequestDispatcher("/WEB-INF/Welcome.jsp");
		view.forward(req, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String eslApiKey = req.getParameter("eslApiKey");
		String eslApiUrl = req.getParameter("eslApiUrl");
		String autoSubmitUrl = req.getParameter("autoSubmitUrl");
		
		HttpSession session = req.getSession();
		
		if(eslApiKey != null && !eslApiKey.isEmpty()){
			session.setAttribute("eslApiKey", eslApiKey);
		}
		
		if(eslApiUrl != null && !eslApiUrl.isEmpty()){
			session.setAttribute("eslApiUrl", eslApiUrl);
		}
		
		if(autoSubmitUrl != null && !autoSubmitUrl.isEmpty()) {
			session.setAttribute("autoSubmitUrl", autoSubmitUrl);
		}
		
		RequestDispatcher view = req.getRequestDispatcher("/WEB-INF/InsuranceForm.jsp");
		view.forward(req, resp);
	}
}
