package com.silanis.esl.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Displays an "Interrupted Signing Page" for packages with statuses other than PACKAGE_COMPLETE and SIGNER_COMPLETE
public class InterruptedSigningController extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	public InterruptedSigningController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/InterruptedSigning.jsp");
		view.forward(request, response);
	}
}