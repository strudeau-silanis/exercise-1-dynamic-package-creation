package com.silanis.esl.controllers;

import javax.servlet.http.HttpServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.silanis.esl.services.DownloadDocumentsService;

//Displays the signed documents in a zip file for view or download

public class DownloadDocumentsController extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	public DownloadDocumentsController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String packageId = request.getParameter("packageId");
		if (packageId == null || packageId.isEmpty()) {
			throw new RuntimeException("packageId parameter is empty or null, please provide a valid package id");
		}

		HttpSession session = request.getSession();
		String eslApiKey = (String)session.getAttribute("eslApiKey");
		String eslApiUrl = (String)session.getAttribute("eslApiUrl");
		
		DownloadDocumentsService svc = new DownloadDocumentsService();
		byte[] downloadDocs = svc.downloadZippedDocuments(packageId, eslApiKey, eslApiUrl);
		response.setContentType(" application/zip");
		ServletOutputStream out = response.getOutputStream();
		out.write(downloadDocs);
		out.close();
	}

}
