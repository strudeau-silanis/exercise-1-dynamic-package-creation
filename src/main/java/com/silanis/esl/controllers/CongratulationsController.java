package com.silanis.esl.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Displays the "Congratulations Page" of the insurance company web application 
public class CongratulationsController extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	public CongratulationsController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/Congratulations.jsp");
		view.forward(request, response);
	}
}