package com.silanis.esl.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.silanis.esl.services.EvidenceService;

//Displays the Evidence Summary in the form of a PDF file.
public class EvidenceController extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	public EvidenceController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String packageId = request.getParameter("packageId");
		if (packageId == null || packageId.isEmpty()) {
			throw new RuntimeException("packageId parameter is empty or null, please provide a valid package id");
		}

		HttpSession session = request.getSession();
		String eslApiKey = (String)session.getAttribute("eslApiKey");
		String eslApiUrl = (String)session.getAttribute("eslApiUrl");
		
		EvidenceService svc = new EvidenceService();
		byte[] evidenceSummary = svc.downloadEvidenceSummary(packageId, eslApiKey, eslApiUrl);
		response.setContentType("application/pdf");
		response.setContentLength(evidenceSummary.length);
		ServletOutputStream out = response.getOutputStream();
		out.write(evidenceSummary);
		out.close();
	}

}
