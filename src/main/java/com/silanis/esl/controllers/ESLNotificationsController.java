package com.silanis.esl.controllers;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.silanis.esl.Database.NotificationsDatabase;
import com.silanis.esl.models.ESLNotificationsModel;
import com.silanis.esl.services.ESLNotificationsService;


//Displays the e-SignLive event notifications
//Displays the status of the packages
public class ESLNotificationsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger log = LoggerFactory.getLogger(ESLNotificationsController.class);
  
	public ESLNotificationsController() {
		super();
	}

	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException {
		try{
			//TODO: Uncomment the logging statement below and deploy your application to the cloud and see it in action!
			//log.info("received notification");
		
			ServletInputStream jsonInputStream = request.getInputStream();
			
			ObjectMapper mapper = new ObjectMapper();
			final ObjectReader objectReader = mapper.reader(ESLNotificationsModel.class);
			ESLNotificationsModel eslNotification = objectReader.without(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).readValue(jsonInputStream);
	        
	        ESLNotificationsService svc = new ESLNotificationsService();
	        svc.saveToDB(eslNotification);
	      
		}catch(Exception e)
		{
			log.error("Failure to handle eSL notification", e);
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Get the notification
		NotificationsDatabase database = NotificationsDatabase.getInstance();
		
		
		ConcurrentMap<String, String> myMap = database.getMap();
		
		request.setAttribute("myMap", myMap);
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ESLNotifications.jsp");
		view.forward(request, response);
	}
}
