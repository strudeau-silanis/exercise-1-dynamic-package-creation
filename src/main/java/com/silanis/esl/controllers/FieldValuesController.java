package com.silanis.esl.controllers;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.silanis.esl.services.FieldValuesService;

// Displays the values found in the fields of signed and completed documents 
public class FieldValuesController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	public FieldValuesController() {
        super();
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String packageId = request.getParameter("packageId");
		if(packageId == null || packageId.isEmpty()) {
			throw new RuntimeException("packageId parameter is empty or null, please provide a valid package id");
		}
		
		HttpSession session = request.getSession();
		String eslApiKey = (String)session.getAttribute("eslApiKey");
		String eslApiUrl = (String)session.getAttribute("eslApiUrl");
		
		FieldValuesService svc= new FieldValuesService();
		String fieldValues= svc.getFieldValues(packageId, eslApiKey, eslApiUrl);
		response.setContentType("text/plain");
	
		PrintWriter out = response.getWriter(); 
		out.print(fieldValues); 
	    out.close(); 
	}
}
