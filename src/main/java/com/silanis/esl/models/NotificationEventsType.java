package com.silanis.esl.models;

public enum NotificationEventsType {
    PACKAGE_CREATE,
	PACKAGE_ACTIVATE,
    PACKAGE_COMPLETE,
    PACKAGE_EXPIRE,
    PACKAGE_OPT_OUT,
    PACKAGE_DECLINE,
    SIGNER_COMPLETE,
    DOCUMENT_SIGNED,
    ROLE_REASSIGN,
    
}

