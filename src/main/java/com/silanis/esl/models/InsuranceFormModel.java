package com.silanis.esl.models;

public class InsuranceFormModel {

	private String insuredInitials;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String province;
	private String emailAddress;
	private String language;
	private String vehicleModel;
	private String vehicleMake;
	private String vehicleModelYear;
	private String vehicleColor;
		
	// insured Initials
	public String getInsuredInitials() {
		return insuredInitials;
	}

	public void setInsuredInitials(String insuredInitials) {
		this.insuredInitials = insuredInitials;
	}

	// first name
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	// last name
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	// address
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// city
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	// province
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	// email address
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	// language
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	// vehicle model
	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	// vehicle make
	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	// vehicle Model Year
	public String getVehicleModelYear() {
		return vehicleModelYear;
	}

	public void setVehicleModelYear(String vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}

	// vehicle color
	public String getVehicleColor() {
		return vehicleColor;
	}

	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}

}