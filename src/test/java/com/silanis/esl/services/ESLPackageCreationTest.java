package com.silanis.esl.services;

import static com.silanis.esl.services.ESLPackageCreation.createPackage;
import static org.junit.Assert.assertNotNull;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.silanis.esl.models.InsuranceFormModel;
import com.silanis.esl.sdk.EslException;
import com.silanis.esl.sdk.PackageId;

public class ESLPackageCreationTest {
	
	private String eSLApiKey;
	private String eSLApiUrl;
	private String autoSubmitUrl;
	
	@Before
	public void setup(){
		Properties properties = System.getProperties();
		properties.setProperty("CallBackURL", "http://localhost:8080/sample-insurance-app/eslNotifications");
		properties.setProperty("java.util.logging.config.file", "C:/silanis/workspace/vegasProject/sample-insurance-app/src/test/resources/logging.properties");
		eSLApiKey = properties.getProperty("eSLApiKey");
		eSLApiUrl = properties.getProperty("eSLApiUrl");
		autoSubmitUrl = properties.getProperty("autoSubmitUrl");
	}

	@Test
	public void successfulPackageCreationTest() {
		InsuranceFormModel model = new InsuranceFormModel();
		model.setFirstName("Mary");
		model.setLastName("Smith");
		model.setEmailAddress("mary.smith.test@mailinator.com");
		PackageId packageId = createPackage(model, eSLApiKey, eSLApiUrl, autoSubmitUrl);
		System.out.println(packageId);
		assertNotNull(packageId);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void missingEmailTest() {
		InsuranceFormModel model = new InsuranceFormModel();
		model.setFirstName("Mary");
		model.setLastName("Smith");
		
		createPackage(model, eSLApiKey, eSLApiUrl, autoSubmitUrl);
	}
	
	@Test(expected=EslException.class)
	public void missingFirstNameTest() {
		InsuranceFormModel model = new InsuranceFormModel();
		
		model.setLastName("Smith");
		model.setEmailAddress("mary.smith.test@mailinator.com");
		createPackage(model, eSLApiKey, eSLApiUrl, autoSubmitUrl);
	}
	
	@Test(expected=EslException.class)
	public void missingLastNameTest() {
		InsuranceFormModel model = new InsuranceFormModel();
		model.setFirstName("Mary");
		
		model.setEmailAddress("mary.smith.test@mailinator.com");
		createPackage(model, eSLApiKey, eSLApiUrl, autoSubmitUrl);
	}
}
